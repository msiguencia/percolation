/**
 * An implementation of a percolation model.
 * <p>
 * This data structure encapsulates any properties that an n-by-n percolation
 * grid may have and supports several basic operations on it.
 * <p>
 * A percolation system, here, is modeled by a grid of n-by-n sites where a site
 * can be either blocked or open. A full site is any site that is open and
 * connected to an open site in the top row via a chain of adjacent
 * (top, bottom, left, right) open sites. A system percolates if there is a full
 * site in the bottom row. In other words, if we fill all open sites connected
 * to top row, then a system percolates. One can think of this grid as a porous
 * material where the open sites correspond to empty space in the material
 * allowing some fluid to flow through, so that when the fluid flows from top to
 * bottom, the system percolates.
 * @author Marco Siguencia
 */
public class Percolation{
    
    private boolean[][] grid;
    private WeightedQuickUnionUF unionFind;
    private WeightedQuickUnionUF noBackwash;
    private int n;
    private int vTop;
    private int vBottom;

    /**
     * Sets all sites in the n-by-n percolation grid to blocked, initializes the
     * union-find structure, the Weighted Quick-Union structure specifically,
     * responsible for keeping track of the connected sites, and sets the
     * virtual nodes to the appropriate index, namely 0 and n*n+1.
     */
    public Percolation(int n){
        this.n = n;
        setGrid(n+1);
        unionFind = new WeightedQuickUnionUF((n*n)+2);
        noBackwash = new WeightedQuickUnionUF((n*n)+1);
        vTop = 0;
        vBottom = n*n+1;
    }

    /**
     * Creates and sets the n-by-n percolation grid, where n is the provided
     * dimensionality, to blocked.
     *
     * @param n the dimensions of a n-by-n grid
     */
    private void setGrid(int n){
        grid = new boolean[n][n];
    }
    /**
     * Maps a site from the percolation grid to a segment of a one dimensional
     * array of size n. Note: It is assumed that the beginning and end positions
     * of the one dimensional array correspond to the virtual nodes.
     *
     * @param i row index
     * @param j column index
     * @return a position in the one dimensional array corresponding to i,j
     */
    private int map(int i, int j){
        return (n * i) - (n - j);
    }
    /**
     * Opens a blocked site and connects it to any open adjacent sites that are
     * to the top, bottom, left and right of it. In addition, it connects open
     * sites in the top row and the bottom row to their respective virtual node.
     * @param i the row index greater than 0 but less than or equal to n
     * @param j the column index greater than 0 but less than or equal to n
     * @throws IndexOutOfBoundsException when an index is out of bounds, that
     * is, less than 1 or greater than n
     */
    public void open(int i, int j){

        if (outOfBounds(i, j)) 
            throw new IndexOutOfBoundsException("One or both indices (" + i 
                    + ", " + j + ") are out of range. Indices must be from" 
                    + " 1 to n.");
        
        if (isOpen(i, j))
            return;

        grid[i][j] = true;
        
        //connect elements in the first row to the top virtual node
        if (i == 1){
            unionFind.union(map(i, j), vTop);
            noBackwash.union(map(i, j), vTop);
        }
        //connect elements in the last row to the bottom virtual node
        if (i == n) unionFind.union(map(i, j), vBottom);

        //connect the adjacent, open nodes
        //top
        if (i != 1 && grid[i-1][j] != false){
            unionFind.union(map(i, j), map(i-1, j));
            noBackwash.union(map(i, j), map(i-1, j));
        }
        //left
        if (j != 1 && grid[i][j-1] != false){
            unionFind.union(map(i, j), map(i, j-1));
            noBackwash.union(map(i, j), map(i, j-1));
        }
        //right
        if (j != n && grid[i][j+1] != false){
            unionFind.union(map(i, j), map(i, j+1));
            noBackwash.union(map(i, j), map(i, j+1));
        }
        //bottom
        if (i != n && grid[i+1][j] != false){
            unionFind.union(map(i, j), map(i+1, j));
            noBackwash.union(map(i, j), map(i+1, j));
        }
    }
    /**
     * Checks if a site in the percolation grid is open or blocked.
     * @param i row index
     * @param j column index
     * @return True if a site is open, otherwise False
     * @throws IndexOutOfBoundsException when an index is out of bounds, that
     * is, less than 1 or greater than n
     */
    public boolean isOpen(int i, int j){
        if (outOfBounds(i, j)) 
            throw new IndexOutOfBoundsException("One or both indices (" + i 
                    + ", " + j + ") are out of range. Indices must be from" 
                    + " 1 to n.");
        return grid[i][j] == true;
    }
    /**
     * Checks if a site is full.
     *
     * @param i the row index greater than 0 but less than or equal to n
     * @param j the column index greater than 0 but less than or equal to n
     * @return True if a site is full, otherwise False
     * @throws IndexOutOfBoundsException when an index is out of bounds, that
     * is, less than 1 or greater than n
     */
    public boolean isFull(int i, int j){
        if (outOfBounds(i, j)) 
            throw new IndexOutOfBoundsException("One or both indices (" + i 
                    + ", " + j + ") are out of range. Indices must be from" 
                    + " 1 to n.");
        return isOpen(i, j) && noBackwash.connected(vTop, map(i, j));
    }
    /**
     * Checks if the system percolates. In other words, it checks if a site at
     * the last row of the grid is full. This method makes use of virtual nodes
     * to improve the running time of this operation.
     * @return True if a system percolates, otherwise False
     */
    public boolean percolates(){
        return unionFind.connected(vTop, vBottom);
    }
    /**
     * Checks if the indices are out of bounds. Note that the percolation system
     * uses indices that range from 1 to n.
     *
     * @param i row index
     * @param j column index
     * @return True if either or both indices are out of bounds, otherwise False
     */
    private boolean outOfBounds(int i, int j){
        return (i < 1 || i > n) || (j < 1 || j > n);  
    }
}
