/** 
 * Performs a Monte Carlo simulation to estimate the percolation threshold.
 * The following computation experiment repeats T times:
 * <ul>
 * <li>All sites are initialized to a blocked state.</li>
 * <li>The following repeats until the system percolates:
 *      <ul>
 *          <li> Choose a site (with indices row i, column j) uniformly at 
 *          random among all currently blocked sites.</li>
 *          <li>Open the chosen site.</li>
 *      </ul>
 *</li>
 *<li>The fraction of sites opened, from the entire percolation grid, provides
 *     an estimate of the percolation threshold.</li>
 *</ul>
 * 
 * <p>
 * Using the results of these experiments, the sample data, several methods 
 * compute statistical properties that provide some insight about the actual 
 * percolation threshold. The sample mean, an average of all the results, 
 * provides a more accurate estimate of the percolation threshold. The sample 
 * standard deviation measures the sharpness of the threshold. And finally, the 
 * 95% confidence interval indicates the reliability of the estimation, provided
 * that T is greater than or equal to 30.  
 * </p>
 *
 * @author Marco Siguencia
 *
 */
public class PercolationStats{
    
    private int trials;
    private int n;
    private double[] trialResults;
    private double mean;
    private double stdDev;

    /**
     * PercolationStats constructor.
     * @param N the dimension of the N-by-N grid
     * @param T the number of times to repeat the experiment
     * @throws IllegalArgumentException when N or T is less than 1
     */
    public PercolationStats(int N, int T){
        if (N < 1 || T < 1) 
            throw new IllegalArgumentException("The number of trials and the " 
                    + "dimensionality of the percolation system must be => 1.");
        this.n = N;
        trials = T;
        trialResults = new double[trials];
        performTrials();
    }

    /**
     * Performs trials of the experiment and records the results.
     */
    private void performTrials(){
        for (int i = 0; i < trials; i++)
            trialResults[i] = experiment();
    }
    /**
     * Performs a single experiment.
     * Consider the following experiment:
     * <ul>
     *      <li>All sites are initialized to a blocked state.</li>
     *      <li>
     *          <ul>The following repeats until the system percolates:
     *              <li>Choose a site (with indices row i, column j) uniformly 
     *                  at random among all currently blocked sites.</li>
     *              <li>Open the chosen site.</li>
     *          </ul>
     *      </li> 
     *      <li>The fraction of sites opened, from the entire percolation grid, 
     *      provides an estimate of the percolation threshold.</li>
     * </ul>
     * 
     * @return the fraction of sites opened from the entire percolation grid
     */
    private double experiment(){
        //For when we have only one open site.
        if (n == 1)
            return 1.0;
        Percolation p = new Percolation(n);
        int openSites = 0;
        while (!p.percolates()){
            int i = StdRandom.uniform(1, n+1);
            int j = StdRandom.uniform(1, n+1);
            if (!p.isOpen(i, j)){
                p.open(i, j);
                openSites++;
            }
        }
        return (double) openSites/(n*n);
    }
    /**
     * Calculates the mean using results from the experimental trials.
     *
     * @return mean
     */
    public double mean(){
        if (mean != 0.0d)
            return mean;
        return mean = StdStats.mean(trialResults);
    }
    /**
     * Calculates the standard deviation using results from the experimental 
     * trials.
     *
     * @return the standard deviation (if the number of trials is less than or
     * equal to 1 then the standard deviation is Double.NaN)
     */
    public double stddev(){
        if (stdDev != 0.0d)
            return stdDev;
        //So that we don't divide by 0.
        if (trials <= 1)
            return stdDev = Double.NaN;
        return stdDev = StdStats.stddev(trialResults);
    }
    /**
     * Calculates the lower bound of the confidence interval based on the
     * results from the experimental trials.
     *
     * @return the lower bound of the confidence interval
     */

    public double confidenceLo(){
        return mean()-(1.96*stddev()/Math.sqrt(trials));
    }

    /**
     * Calculates the upper bound of the confidence interval based on the
     * results from the experimental trials.
     *
     * @return the upper bound of the confidence interval
     */

    public double confidenceHi(){
        return mean()+(1.96*stddev()/Math.sqrt(trials));
    }

    /**
     * Instantiates PercolationStats using the command line arguments provided 
     * and prints the statistical properties for the percolation threshold.
     * 
     * @param args the two command line arguments must be N and T in that
     * order, where N is the dimensionality of a N-by-N percolation grid and T 
     * is the number of trials of the experiment that is to be performed 
     */
    public static void main(String[] args){

        PercolationStats stats = new PercolationStats(Integer.parseInt(args[0]),
                Integer.parseInt(args[1]));
        System.out.println("Mean: " + stats.mean());
        System.out.println("StdDev: " + stats.stddev());
        System.out.println("95% confidence interval: " + stats.confidenceLo() + 
                ", " + stats.confidenceHi());
    }
}
